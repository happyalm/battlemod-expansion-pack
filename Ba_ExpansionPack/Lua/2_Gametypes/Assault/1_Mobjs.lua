freeslot('mt_assaultpoint')

// Assault point object
mobjinfo[MT_ASSAULTPOINT] = {
	//$Name "Assault Point"
	//$Sprite EMBMC0
	//$Category "BattleMod Assault Point"
	doomednum = 3641,
	spawnstate = S_EMBLEM3,
	height = 32*FRACUNIT,
	radius = 24*FRACUNIT,
	flags = MF_NOGRAVITY|MF_SCENERY
}