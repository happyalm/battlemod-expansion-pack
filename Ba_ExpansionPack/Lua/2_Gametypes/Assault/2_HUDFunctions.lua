local B = CBW_Battle
local A = B.Assault
local CV = B.Console
local AGS = A.GameState

A.HUD = function(v)
	if gametype != GT_ASSAULT
	or B.PreRoundWait()
		return
	end
	
	local flags = V_HUDTRANS|V_SNAPTOTOP|V_PERPLAYER
	local x = 160
	local y = 16
	
	--Draw points on the hud
	for i = 0, 4
		local point = AGS.Points[i]
		if not (point and point.valid)
			continue
		end
		local team = point.team
		local spacing = 13
		local offset = -(2 - i) * spacing
		local patch = "APOINT00"
		local scale = FRACUNIT * 2/3
		local color
		if team == 1
			color = v.getColormap(TC_DEFAULT, SKINCOLOR_RED)
		elseif team == 2
			color = v.getColormap(TC_DEFAULT, SKINCOLOR_BLUE)
		else
			color = v.getColormap(TC_DEFAULT, SKINCOLOR_GREY)
		end
		if not point.locked
			patch = "APOINT20"
		end
		v.drawScaled((x + offset) * FRACUNIT, y * FRACUNIT, scale, v.cachePatch(patch), flags, color)
		if point.meter != 0 and not point.locked
			if point.meter > 0
				color = v.getColormap(TC_DEFAULT, SKINCOLOR_RED)
			elseif point.meter < 0
				color = v.getColormap(TC_DEFAULT, SKINCOLOR_BLUE)
			end
			local progress = abs(point.meter) * 20 / FRACUNIT
			patch = "APOINT"+progress
			v.drawScaled((x + offset) * FRACUNIT, y * FRACUNIT, scale, v.cachePatch(patch), flags, color)
		end
	end
	
	--Unlock timer
	if AGS.UnlockTimer >= 0
		local num = CV.AssaultLockTimer.value - (AGS.UnlockTimer / TICRATE)
		if (num > 5 or AGS.UnlockTimer % 2)
			v.drawString(x, y + 8, num, flags, "center")
		end
	end
end