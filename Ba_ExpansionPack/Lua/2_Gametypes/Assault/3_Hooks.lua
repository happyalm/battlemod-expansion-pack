local B = CBW_Battle
local A = B.Assault

addHook("MapChange", A.Init)
addHook("MapThingSpawn", A.MapThingSpawn, MT_ASSAULTPOINT)
addHook("ThinkFrame", A.ThinkFrame)
hud.add(A.HUD, "game")