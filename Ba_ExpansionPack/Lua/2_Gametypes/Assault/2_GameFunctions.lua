local B = CBW_Battle
local A = B.Assault
local CV = B.Console
A.WinSFX = sfx_kc42--sfx_cdfm73
A.LoseSFX = sfx_kc46
A.HintSFX = sfx_prloop
A.UnlockSFX = sfx_cdfm44
A.StartCaptureSFX = sfx_drill1
A.CapturingSFX = sfx_drill2
A.GameState = {}
local AGS = A.GameState

A.ThinkFrame = function()
	if gametype != GT_ASSAULT
	or B.PreRoundWait()
		return
	end
	if AGS.UnlockTimer >= 0 and AGS.UnlockTimer < CV.AssaultLockTimer.value * TICRATE
		AGS.UnlockTimer = $ + 1
		if AGS.UnlockTimer == (CV.AssaultLockTimer.value - 5) * TICRATE
			S_StartSound(nil, A.HintSFX)
		end
		if AGS.UnlockTimer == CV.AssaultLockTimer.value * TICRATE
			AGS.UnlockTimer = -1
			S_StartSound(nil, A.UnlockSFX);
			COM_BufInsertText(server, "csay Capture Point\\Unlocked!\\\\\\\\")
			local point = AGS.Points[2]
			if point and point.valid
				point.locked = false
			end
		end
	end
end

A.Init = function()
	if gametype != GT_ASSAULT
		hud.enable("teamscores")
		return
	end
	hud.disable("teamscores")
	AGS.Points = {}
	AGS.UnlockTimer = 0
	redscore = 2
	bluescore = 2
end

A.MapThingSpawn = function(mo, thing)
	if gametype != GT_ASSAULT
		P_RemoveMobj(mo)
		return
	end
	local id = thing.extrainfo
	mo.id = id
	mo.locked = true
	mo.meter = 0
	if id < 2
		mo.team = 1
	elseif id == 2
		mo.team = 0
	else
		mo.team = 2
	end
	AGS.Points[thing.extrainfo] = mo
	print("Assigned point "+id+" to team "+mo.team)
end

A.SeizePoint = function(point, team)
	point.team = team
	if team == 1--Red
		print("\x85 Red Team captured the Control Point!")
		redscore = $ + 1
		if (point.team != 0)
			bluescore = $ - 1
		end
		if (point.id < 4)
			AGS.Points[point.id + 1].locked = false
		end
		AGS.Points[point.id - 1].locked = true
	elseif team == 2--Blue
		print("\x84 Blue Team captured the Control Point!")
		bluescore = $ + 1
		if (point.team != 0)
			redscore = $ - 1
		end
		if (point.id > 0)
			AGS.Points[point.id - 1].locked = false
		end
		AGS.Points[point.id + 1].locked = true
	end
	if (!consoleplayer or consoleplayer.spectator or consoleplayer.ctfteam == team)
		S_StartSound(nil, A.WinSFX)
	else
		S_StartSound(nil, A.LoseSFX)
	end
end