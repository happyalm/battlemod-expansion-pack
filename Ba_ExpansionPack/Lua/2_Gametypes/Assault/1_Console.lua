local B = CBW_Battle
local CV = B.Console

CV.AssaultLockTimer = CV_RegisterVar{
	name = "assault_locktimer",
	defaultvalue = 20,
	flags = CV_NETVAR,
	PossibleValue = {MIN = 0, MAX = 60}
}