local B = CBW_Battle
addHook("NetVars", function(n)
	B.Assault.GameState = n($)
	B.Domination.GameState = n($)
	B.CTF.GameState = n($)
end)