local B = CBW_Battle
local C = B.CTF
local CGS = C.GameState

addHook("MapThingSpawn", function(mobj, mt)
	CGS.flagspawns[1] = mt
end, MT_REDFLAG)
addHook("MapThingSpawn", function(mobj, mt)
	CGS.flagspawns[2] = mt
end, MT_BLUEFLAG)
addHook("MobjThinker", C.TrackRedFlag, MT_REDFLAG)
addHook("MobjThinker", C.TrackBlueFlag, MT_BLUEFLAG)
addHook("MapChange", C.Init)
addHook("ThinkFrame", C.ThinkFrame)
hud.add(C.HUD, "game")