local B = CBW_Battle
local C = B.CTF
local CGS = C.GameState

C.HUD = function(v)
	if gametype != GT_BATTLECTF
		return
	end
	
	--An attempt to look exactly like the hardcode cecho
	if CGS.CaptureHUDTimer
		local trans = 0
		if (CGS.CaptureHUDTimer <= 20)
			trans = V_10TRANS * ((20 - CGS.CaptureHUDTimer) / 2)
		end
		local name = CGS.CaptureHUDName
		local team = CGS.CaptureHUDTeam
		local red = "\x85"
		local blue = "\x84"
		local flagtext
		if (team == 1)
			name = red + $
			flagtext = blue+"BLUE FLAG"
		else
			name = blue + $
			flagtext = red+"RED FLAG"
		end
		local x = 160
		local y = 66
		v.drawString(x, y, name, trans|V_ALLOWLOWERCASE, "center")
		v.drawString(x, y + 12, "CAPTURED THE "+flagtext+"\x80.", trans, "center")
		CGS.CaptureHUDTimer = $ - 1
	end
	
	if consoleplayer and consoleplayer.ctf_slowcap
		local x = 160
		local y = 186
		local width = (6 * TICRATE - consoleplayer.ctf_slowcap) / 6
		local flags = V_HUDTRANS|V_SNAPTOBOTTOM|V_PERPLAYER
		v.drawFill(x - width, y, width * 2, 4, flags | (72 + consoleplayer.ctf_slowcap % 8))
		v.drawString(x, y - 8, "DELAYED CAPTURE", flags, "thin-center")
	end
end