local B = CBW_Battle
local C = B.CTF
C.WinSFX = sfx_flgcap
C.LoseSFX = sfx_lose
C.GameState = {
	flagspawns = {},
	redflag = nil,
	blueflag = nil
}
local CGS = C.GameState

C.Init = function()
	if gametype != GT_BATTLECTF
		return
	end
	CGS.flagspawns[1] = nil
	CGS.flagspawns[2] = nil
	CGS.CaptureHUDTimer = nil
	CGS.CaptureHUDName = nil
	CGS.CaptureHUDTeam = nil
end

C.TrackRedFlag = function(mo)
	CGS.redflag = mo
end
C.TrackBlueFlag = function(mo)
	CGS.blueflag = mo
end

C.ThinkFrame = function()
	if gametype != GT_BATTLECTF
		return
	end
	for player in players.iterate()
		if player.spectator or not (player.mo and player.mo.valid and player.gotflag and player.ctfteam)
			player.ctf_slowcap = nil
			continue
		end
		local mo = player.mo
		if player.gotflag == GF_BLUEFLAG
			CGS.blueflag = mo
		elseif player.gotflag == GF_REDFLAG
			CGS.redflag = mo
		end
		local team = player.ctfteam
		local otherteam = 2
		if (team == 2)
			otherteam = 1
		end
		local effect = 3
		local homeflag = MT_REDFLAG
		local capturedflag = MT_BLUEFLAG
		if (team == 2)--player is on blue team
			effect = 4
			homeflag = MT_BLUEFLAG
			capturedflag = MT_REDFLAG
		end
		if P_PlayerTouchingSectorSpecial(player, 4, effect) and P_IsObjectOnGround(mo) and not P_IsFlagAtBase(homeflag)
			local cap = false
			if B.Overtime
				cap = true
			else
				if player.ctf_slowcap == nil
					player.ctf_slowcap = 1
				else
					player.ctf_slowcap = $ + 1
				end
				if player.ctf_slowcap == 6 * TICRATE
					cap = true
				end
			end
			if cap
				player.ctf_slowcap = nil
				if (consoleplayer)
					local colstr = "\205RED"
					local name = player.name
					if team == 1
						colstr = "\204BLUE"
					end
				end
				CGS.CaptureHUDTimer = 5*TICRATE
				CGS.CaptureHUDName = player.name
				CGS.CaptureHUDTeam = team
				
				if splitscreen or (consoleplayer and consoleplayer.ctfteam == team)
					S_StartSound(nil, C.WinSFX)
				else
					S_StartSound(nil, C.LoseSFX)
				end
				
				local flagmo = P_SpawnMobj(mo.x,mo.y,mo.z,capturedflag)
				player.gotflag = 0
				flagmo.flags = $ & ~MF_SPECIAL
				flagmo.fuse = 6 * TICRATE
				flagmo.spawnpoint = CGS.flagspawns[otherteam]
				flagmo.flags2 = $ | MF2_JUSTATTACKED
				if (team == 1)
					redscore = $ + 1
				else
					bluescore = $ + 1
				end
				P_AddPlayerScore(player, 250)
			elseif player.ctf_slowcap % 39 == 1
				S_StartSoundAtVolume(nil, sfx_s3kc3s, 60)
			elseif player.ctf_slowcap % 39 == 13
				S_StartSoundAtVolume(nil, sfx_s3kc3s, 120)
			elseif player.ctf_slowcap % 39 == 26
				S_StartSoundAtVolume(nil, sfx_s3kc3s, 180)
			end
		else
			player.ctf_slowcap = nil
		end
	end
	if CGS.redflag and not CGS.redflag.valid
		CGS.redflag = nil
	end
	if CGS.blueflag and not CGS.blueflag.valid
		CGS.blueflag = nil
	end
end