local registered = false
addHook("ThinkFrame", do
	if (not registered) and MapVote
		registered = true
		local rg = MapVote.RegisterGametype
		rg(GT_DOMINATION, "Domination", 0, 0, TOL_DOMINATION, TOL_CP)
		rg(GT_ASSAULT, "Assault", 0, 0, TOL_ASSAULT)
	end
end)