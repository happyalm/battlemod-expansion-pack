local B = CBW_Battle
local G = B.Gametypes

B.CTF = {}

B.Domination = {}
freeslot('tol_domination')
G_AddGametype({
	name = "Domination",
	identifier = "domination",
	typeoflevel = TOL_DOMINATION|TOL_CP,
	rules = GTR_OVERTIME|GTR_STARTCOUNTDOWN|GTR_RESPAWNDELAY|GTR_PITYSHIELD|GTR_SPECTATORS|GTR_POINTLIMIT|GTR_TIMELIMIT|GTR_SPAWNINVUL|GTR_DEATHPENALTY|GTR_DEATHMATCHSTARTS|GTR_HURTMESSAGES,
	rankingtype = GT_MATCH,
	intermissiontype = int_match,
	defaultpointlimit = 3000,
	defaulttimelimit = 5,
	headerleftcolor = 75,
	headerrightcolor = 75,
	description = "Race around the map capturing control points! Multiple control points will spawn at once, so think fast!"
})
G.Battle[GT_DOMINATION] = true
G.TeamScoreType[GT_DOMINATION] = 0
G.SuddenDeath[GT_DOMINATION] = 0
G.CP[GT_DOMINATION] = false
G.Arena[GT_DOMINATION] = false
G.Diamond[GT_DOMINATION] = false

B.Assault = {}
freeslot('tol_assault')
G_AddGametype({
	name = "Assault",
	identifier = "assault",
	typeoflevel = TOL_ASSAULT,
	rules = GTR_OVERTIME|GTR_STARTCOUNTDOWN|GTR_RESPAWNDELAY|GTR_PITYSHIELD|GTR_TEAMS|GTR_SPECTATORS|GTR_POINTLIMIT|GTR_TIMELIMIT|GTR_SPAWNINVUL|GTR_DEATHPENALTY|GTR_DEATHMATCHSTARTS|GTR_HURTMESSAGES,
	rankingtype = GT_CTF,
	intermissiontype = int_ctf,
	defaultpointlimit = 3,
	defaulttimelimit = 8,
	headerleftcolor = 37,
	headerrightcolor = 150,
	description = "Each team starts with 2 control points; capture all 5 in a row to win! Advance towards the enemy's base and take their territory!"
})
G.Battle[GT_ASSAULT] = true
G.TeamScoreType[GT_ASSAULT] = 0
G.SuddenDeath[GT_ASSAULT] = 0
G.CP[GT_ASSAULT] = false
G.Arena[GT_ASSAULT] = false
G.Diamond[GT_ASSAULT] = false