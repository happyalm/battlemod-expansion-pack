freeslot('MT_MAPRADARPOINT1','MT_MAPRADARPOINT2')

mobjinfo[MT_MAPRADARPOINT1] = {
	//$Name "Map Radar Point (northwest)"
	//$Sprite ZBRADARD
	//$Category "Map Radar"
	doomednum = 98
}
mobjinfo[MT_MAPRADARPOINT2] = {
	//$Name "Map Radar Point (southeast)"
	//$Sprite ZBRADARD
	//$Category "Map Radar"
	doomednum = 99
}

rawset(_G,"radarpoint1",0)
rawset(_G,"radarpoint2",0)

addHook('NetVars',function(net)
	radarpoint1 = net($)
	radarpoint2 = net($)
end)

addHook("MapLoad",do
	radarpoint1 = 0
	radarpoint2 = 0
	
	for mapthing in mapthings.iterate
		local mt = mapthing
		local t = mt.type
		if t == 98
			radarpoint1 = mt
			--print("found rp1 "..mt.x..", "..mt.y)
		end
		if t == 99
			radarpoint2 = mt
			--print("found rp2 "..mt.x..", "..mt.y)
		end
	end
end)
