local B = CBW_Battle
local C = B.CTF
local CGS = C.GameState
local MINIMAP_XPOS = 320-8
local MINIMAP_YPOS = 142
local BIGMAP_XPOS = 160
local BIGMAP_YPOS = 100

freeslot("spr_radd")

local cv_minimap = CV_RegisterVar{
	name = "minimap",
	defaultvalue = 1,
	flags = 0,
	PossibleValue = CV_OnOff
}

//IntToExtMapNum(n)
//Returns the extended map number as a string
//Returns nil if n is an invalid map number
local function IntToExtMapNum(n)
	if n < 0 or n > 1035
		return nil
	end
	if n < 10
		return "MAP0" + n
	end
	if n < 100
		return "MAP" + n
	end
	local x = n-100
	local p = x/36
	local q = x - (36*p)
	local a = string.char(65 + p)
	local b
	if q < 10
		b = q
	else
		b = string.char(55 + q)
	end
	return "MAP" + a + b
end

//Draw an icon on the map
local function drawicon(v, xoff, yoff, x, y, angle, scale, patch, flags, colormap)
	if angle != nil
		v.drawScaled(
			xoff+x+3*FixedMul(sin(angle+ANGLE_90),scale*3),
			yoff+y+2*FRACUNIT+(FRACUNIT/2)+3*FixedMul(cos(angle+ANGLE_90),scale*3),
			scale,
			v.getSpritePatch(SPR_RADD,0,0,angle),
			flags
		)
		y = $ + FixedMul(scale, 8*FRACUNIT)
		x = $ + FixedMul(scale, FRACUNIT*2/3)
	end
	v.drawScaled(xoff+x, yoff+y, scale, patch, flags, colormap)
end

//Draw map at position & scale
local function drawmap(v, xpos, ypos, map_zoom, icon_scale, snapflags, border)
	local patchname = IntToExtMapNum(gamemap).."R"
	if not (v.patchExists(patchname) and radarpoint1 and radarpoint2) return end
	
	local graphic_width = mapheaderinfo[gamemap].minimapwidth or 200
	local graphic_height = mapheaderinfo[gamemap].minimapheight or 200
	
	local radarpoint1_x = radarpoint1.x * FRACUNIT
	local radarpoint1_y = radarpoint1.y * FRACUNIT
	local radarpoint2_x = radarpoint2.x * FRACUNIT
	local radarpoint2_y = radarpoint2.y * FRACUNIT
	local map_screenwidth = FixedMul(graphic_width*FRACUNIT, map_zoom)
	local map_screenheight = FixedMul(graphic_height*FRACUNIT, map_zoom)
	local radarpoints_width = radarpoint2_x - radarpoint1_x
	local radarpoints_height = radarpoint2_y - radarpoint1_y
	local radarpoints_centerx = (radarpoint1_x + radarpoint2_x) / 2
	local radarpoints_centery = (radarpoint1_y + radarpoint2_y) / 2
	local xoff
	if border
		xoff = (xpos-border)*FRACUNIT-(map_screenwidth/2)
	else
		xoff = xpos*FRACUNIT
	end
	local yoff = ypos*FRACUNIT
	local map_patch = v.cachePatch(patchname)
	
	//Draw the map
	v.drawScaled(xoff, yoff, map_zoom, map_patch, TR_TRANS50|snapflags|V_PERPLAYER)
	
	local myteam
	if G_GametypeHasTeams() and (not splitscreen) and consoleplayer and consoleplayer.ctfteam
		myteam = consoleplayer.ctfteam
	end
	
	//Draw capture points
	if CBW_Battle
		local B = CBW_Battle
		local CP = B.ControlPoint
		for i = 1, #CP.ID
			local mo = CP.ID[i]
			local trans = TR_TRANS60
			local scale = FRACUNIT/10
			local col = v.getColormap(TC_ALLWHITE)
			if CP.Active and CP.Num == i
				if CP.Capturing
					trans = 0
					col = v.getColormap(nil, leveltime % 5 + SKINCOLOR_SUPERSKY1)
					scale = FRACUNIT/5
				else
					trans = TR_TRANS30
					scale = FRACUNIT/5
					col = v.getColormap(nil, SKINCOLOR_BLUE)
				end
			end
			local xpos = mo.x - radarpoints_centerx
			local ypos = mo.y - radarpoints_centery
			local x = FixedMul(map_screenwidth, FixedDiv(xpos,radarpoints_width))
			local y = FixedMul(map_screenheight, FixedDiv(ypos,radarpoints_height))
			drawicon(v, xoff, yoff, x, y, nil, FixedMul(scale, icon_scale), v.cachePatch("RAD_CP"), trans|snapflags|V_PERPLAYER, col)
		end
	end
	
	//Draw Players
	for player in players.iterate
		local angle, trans, scale
		local mo = player.realmo
		local color = v.getColormap(player.skin, player.skincolor)
		
		if mo.flags2 & MF2_DONTDRAW
			continue
		end
		
		if myteam and player.ctfteam != myteam
			continue
		end
		
		if (player != displayplayer) and (player != secondarydisplayplayer)
			if player.spectator
				continue
			end
			trans = TR_TRANS30
			scale = FRACUNIT/4
		else
			trans = 0
			angle = mo.angle
			scale = FRACUNIT/3
		end
		if player.spectator
			color = v.getColormap(TC_ALLWHITE)
			trans = TR_TRANS30
		end
		if mo
			local xpos = mo.x - radarpoints_centerx
			local ypos = mo.y - radarpoints_centery
			local x = FixedMul(map_screenwidth, FixedDiv(xpos,radarpoints_width))
			local y = FixedMul(map_screenheight, FixedDiv(ypos,radarpoints_height))
			if P_PlayerInPain(player) or player.playerstate == PST_DEAD
				trans = TR_TRANS50
				if P_PlayerInPain(player)
					y = $ + ((leveltime%2) * FRACUNIT) - FRACUNIT/2
				end
			end
			drawicon(v, xoff, yoff, x, y, angle, FixedMul(scale, icon_scale), v.getSprite2Patch(player.skin, SPR2_LIFE), trans|snapflags|V_PERPLAYER, color)
			
			if player.powers[pw_invulnerability]
				local frame = leveltime % 32
				local frame2 = (leveltime - 6) % 32
				drawicon(v, xoff, yoff, x, y + 6*FRACUNIT, nil, FixedMul(scale, icon_scale) / 2, v.getSpritePatch(SPR_IVSP, frame), snapflags|V_PERPLAYER, color)
				drawicon(v, xoff, yoff, x, y + 6*FRACUNIT, nil, FixedMul(scale, icon_scale) / 2, v.getSpritePatch(SPR_IVSP, frame2), TR_TRANS30|snapflags|V_PERPLAYER, color)
			end
		end
	end
	
	//Draw flag (not held by player)
	if (CGS.redflag and not CGS.redflag.player and not CGS.redflag.fuse and not (CGS.redflag.flags2 & MF2_DONTDRAW))
		local scale = FRACUNIT/2
		local xpos = CGS.redflag.x - radarpoints_centerx
		local ypos = CGS.redflag.y - radarpoints_centery
		local x = FixedMul(map_screenwidth, FixedDiv(xpos,radarpoints_width))
		local y = FixedMul(map_screenheight, FixedDiv(ypos,radarpoints_height))
		local color = v.getColormap(TC_RAINBOW, SKINCOLOR_RED)
		drawicon(v, xoff, yoff, x, y, nil, FixedMul(scale, icon_scale), v.cachePatch("RAD_FLAG"), snapflags|V_PERPLAYER, color)
	end
	if (CGS.blueflag and not CGS.blueflag.player and not CGS.blueflag.fuse and not (CGS.blueflag.flags2 & MF2_DONTDRAW))
		local scale = FRACUNIT/2
		local xpos = CGS.blueflag.x - radarpoints_centerx
		local ypos = CGS.blueflag.y - radarpoints_centery
		local x = FixedMul(map_screenwidth, FixedDiv(xpos,radarpoints_width))
		local y = FixedMul(map_screenheight, FixedDiv(ypos,radarpoints_height))
		local color = v.getColormap(TC_RAINBOW, SKINCOLOR_BLUE)
		drawicon(v, xoff, yoff, x, y, nil, FixedMul(scale, icon_scale), v.cachePatch("RAD_FLAG"), snapflags|V_PERPLAYER, color)
	end
	
	//Draw flag (held by player)
	if (CGS.redflag and (CGS.redflag.player or CGS.redflag.fuse) and not (CGS.redflag.flags2 & MF2_DONTDRAW))
		local scale = FRACUNIT * 2/7
		local color = v.getColormap(TC_RAINBOW, SKINCOLOR_RED)
		if (leveltime / 4) % 3 == 0 and not CGS.redflag.fuse
			color = v.getColormap(TC_ALLWHITE)
		end
		local xpos = CGS.redflag.x - radarpoints_centerx
		local ypos = CGS.redflag.y - radarpoints_centery
		local x = FixedMul(map_screenwidth, FixedDiv(xpos,radarpoints_width))
		local y = FixedMul(map_screenheight, FixedDiv(ypos,radarpoints_height))
		drawicon(v, xoff, yoff, x, y, nil, FixedMul(scale, icon_scale), v.cachePatch("RAD_FLAG"), snapflags|V_PERPLAYER, color)
	end
	if (CGS.blueflag and (CGS.blueflag.player or CGS.blueflag.fuse) and not (CGS.blueflag.flags2 & MF2_DONTDRAW))
		local scale = FRACUNIT * 2/7
		local color = v.getColormap(TC_RAINBOW, SKINCOLOR_BLUE)
		if (leveltime / 4) % 3 == 0 and not CGS.blueflag.fuse
			color = v.getColormap(TC_ALLWHITE)
		end
		local xpos = CGS.blueflag.x - radarpoints_centerx
		local ypos = CGS.blueflag.y - radarpoints_centery
		local x = FixedMul(map_screenwidth, FixedDiv(xpos,radarpoints_width))
		local y = FixedMul(map_screenheight, FixedDiv(ypos,radarpoints_height))
		drawicon(v, xoff, yoff, x, y, nil, FixedMul(scale, icon_scale), v.cachePatch("RAD_FLAG"), snapflags|V_PERPLAYER, color)
	end
	
	//Draw help ping icon
	for player in players.iterate
		local mo = player.realmo
		if mo and mo.help_ping
			local xpos = mo.x - radarpoints_centerx
			local ypos = mo.y - radarpoints_centery
			local x = FixedMul(map_screenwidth, FixedDiv(xpos,radarpoints_width))
			local y = FixedMul(map_screenheight, FixedDiv(ypos,radarpoints_height))
			
			local alart = v.cachePatch("WHATC0")
			if (leveltime / 4) % 3
				alart = v.cachePatch("WHATD0")
			end
			drawicon(v, xoff, yoff, x, y, angle, icon_scale / 7, alart, snapflags|V_PERPLAYER, nil)
		end
	end
end

//Draw minimap in the bottom right corner
local function minimap(v, player, cam)
	if CBW_Chaos_Library and CBW_Chaos_Library.Gametypes[gametype]
		return
	end
	if (cv_minimap.value)
		drawmap(v, MINIMAP_XPOS, MINIMAP_YPOS, FRACUNIT/3, FRACUNIT, V_SNAPTOBOTTOM|V_SNAPTORIGHT, 10)
	end
end
hud.add(minimap, 'game')
/*
//Draw big map in the center of the screen
local function bigmap(v, player, cam)
	drawmap(v, BIGMAP_XPOS, BIGMAP_YPOS, FRACUNIT*2/3, FRACUNIT*3/2, 0, nil)
end
hud.add(bigmap, 'scores')
//Disable score HUD to make room for big map
hud.disable("rankings")*/